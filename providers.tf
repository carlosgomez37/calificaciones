provider "aws" {
  region  = var.provider_master.region
  profile = var.provider_master.profile
}
