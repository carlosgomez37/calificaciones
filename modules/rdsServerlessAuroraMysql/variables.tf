variable "layer" {
  description = "A unique identifier for the deployment. Used as a prefix for all the Openstack resources."
  type        = string
}

variable "stack_id" {
  description = "A unique identifier for the deployment. Used as a prefix for all the Openstack resources."
  type        = string
}

variable "db_name" {
  description = "Name Database create"
  type        = string
}

variable "db_user" {
  description = "Name SuperAdmin Database"
  type        = string
}

variable "db_password" {
  description = "Password SuperAdmin Database"
  type        = string
}

variable "db_subnets" {
  description = "Array subnets to associate"
  type        = list(string)
  #  default = ["us-west-1a"]
}

variable "cluster_identifier" {
  description = "Name cluster"
  type        = string
}

variable "vpc" {
  description = "ID VPC use security groups"
  type        = string
}

variable "keypempub" {
  description = "File public of key .pem"
  type        = string
}