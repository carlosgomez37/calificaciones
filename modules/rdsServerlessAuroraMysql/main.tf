
resource "aws_security_group" "sg_db" {
  name        = "sgDb_${var.layer}_${var.stack_id}"
  vpc_id      = var.vpc
  description = "Enable access to the RDS DB"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name   = "db_${var.layer}_${var.stack_id}"
    Source = "Terraform"
  }
}

resource "aws_db_subnet_group" "default_subnet_rds" {
  name        = "subnet_rds_${var.layer}_${var.stack_id}"
  description = "subnet_rds_${var.layer}_${var.stack_id}"
  subnet_ids  = var.db_subnets

  tags = {
    Name   = "DB_${var.layer}_${var.stack_id}"
    Source = "Terraform"
  }
}

resource "aws_rds_cluster" "db" {
  cluster_identifier      = var.cluster_identifier
  engine                  = "aurora-mysql"
  engine_mode             = "serverless"
  database_name           = var.db_name
  master_username         = var.db_user
  master_password         = var.db_password
  vpc_security_group_ids  = ["${aws_security_group.sg_db.id}"]
  db_subnet_group_name    = aws_db_subnet_group.default_subnet_rds.name
  backup_retention_period = 30
  preferred_backup_window = "23:00-00:00"
  deletion_protection     = false
  skip_final_snapshot     = true
  scaling_configuration {
    auto_pause               = true
    max_capacity             = 16
    min_capacity             = 2
    seconds_until_auto_pause = 86400
  }
}
