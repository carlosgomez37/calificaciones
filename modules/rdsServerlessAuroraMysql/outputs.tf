# outputs.tf

output "rds_host_url" {
  value = aws_rds_cluster.db.endpoint
}
