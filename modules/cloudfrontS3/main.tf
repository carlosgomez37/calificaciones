resource "aws_s3_bucket" "s3" {
  bucket = "${var.bucket_name}-${var.stack_id}-x-y-z"
  acl    = "private"

  tags = {
    Name        = "${var.bucket_name}_${var.layer}_${var.stack_id}"
    environment = var.stack_id
    source      = "Terraform"
  }
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.s3.bucket_regional_domain_name
    origin_id   = "s3-${aws_s3_bucket.s3.bucket}"

    # s3_origin_config {
    #   origin_access_identity = "origin-access-identity/cloudfront/ABCDEFG1234567"
    # }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  enabled         = true
  is_ipv6_enabled = false
  comment         = "${var.bucket_name}_${var.layer}_${var.stack_id}"

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD", "OPTIONS"]
    target_origin_id       = "s3-${aws_s3_bucket.s3.bucket}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    compress    = true
    min_ttl     = 0
    default_ttl = 3600
    max_ttl     = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Name        = "${var.bucket_name}_${var.layer}_${var.stack_id}"
    Environment = var.stack_id
    Source      = "Terraform"
  }
}
