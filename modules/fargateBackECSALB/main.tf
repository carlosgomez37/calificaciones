# ALB Security Group: Edit to restrict access to the application
resource "aws_security_group" "lb_fargate" {
  name        = "load_balancer_security_group_frontend_${var.layer}_${var.stack_id}"
  description = "controls access to the ALB"
  vpc_id      = var.vpc

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Traffic to the ECS cluster should only come from the ALB
resource "aws_security_group" "ecs_tasks_fargate" {
  name        = "ecs_tasks_security_group_front${var.layer}_${var.stack_id}"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc

  ingress {
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.lb_fargate.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS task execution role data
data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid     = ""
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# ECS task execution role
resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "FrontEcsTaskExecutionRole_${var.layer}_${var.stack_id}"
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

# ECS task execution role policy attachment
resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

#Create ECR repository
resource "aws_ecr_repository" "ecr" {
  name                 = "ecr_${var.layer}_${var.stack_id}"
  image_tag_mutability = "MUTABLE"

  tags = {
    Name   = "ecr_${var.layer}_${var.stack_id}"
    Source = "Terraform"
  }
}

# Create ALB
resource "aws_alb" "main" {
  name            = replace("balancer_${var.layer}_${var.stack_id}", "_", "-")
  subnets         = var.db_subnets_public
  security_groups = [aws_security_group.lb_fargate.id]
}

resource "aws_alb_target_group" "app" {
  name        = replace("target_group_${var.layer}_${var.stack_id}", "_", "-")
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200,300,301,302"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.main.id
  port              = var.app_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }
}
# RULE BACKEND
resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_alb_listener.front_end.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.app.arn
  }

  condition {
    path_pattern {
      values = ["/api/*"]
    }
  }
}

# Create ECS cluster
resource "aws_ecs_cluster" "main" {
  name = "cluster_front_${var.layer}_${var.stack_id}"
}

data "template_file" "myapp" {
  template = file("./modules/fargateBackECSALB/templates/ecs/myapp.json.tpl")

  vars = {
    #app_image               = "${aws_ecr_repository.ecr.repository_url}:latest"
    app_image               = var.app_image
    app_port                = var.app_port
    fargate_cpu             = var.fargate_cpu
    fargate_memory          = var.fargate_memory
    region                  = var.region
    layer                   = var.layer
    stack_id                = var.stack_id
    user_s3_access_key_secret = var.user_s3_access_key_secret
    user_s3_access_key_id     = var.user_s3_access_key_id
  }
}

resource "aws_ecs_task_definition" "app" {
  family                   = "task_${var.layer}_${var.stack_id}"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.myapp.rendered
}

resource "aws_ecs_service" "main" {
  name            = "service_${var.layer}_${var.stack_id}"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks_fargate.id]
    subnets          = var.db_subnets_private
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.app.id
    container_name   = "container_${var.layer}_${var.stack_id}"
    container_port   = var.app_port
  }

  depends_on = [aws_alb_listener.front_end, aws_iam_role_policy_attachment.ecs_task_execution_role]
}