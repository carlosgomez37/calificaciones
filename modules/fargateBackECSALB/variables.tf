variable "layer" {
  description = "A unique identifier for the deployment. Used as a prefix for all the Openstack resources."
  type        = string
}

variable "stack_id" {
  description = "A unique identifier for the deployment. Used as a prefix for all the Openstack resources."
  type        = string
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  type        = string
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  type        = number
}

variable "app_count" {
  description = "Number of docker containers to run"
  type        = number
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  type        = string
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  type        = string
}

variable "region" {
  description = "Region where the infra is displayed"
  type        = string
}

variable "db_subnets_public" {
  description = "Array subnets publics to associate"
  type        = list(string)
}

variable "db_subnets_private" {
  description = "Array subnets publics to associate"
  type        = list(string)
}

variable "vpc" {
  description = "ID VPC use security groups"
  type        = string
}

variable "ecr_repository" {
  description = "ECR repositories for ECS"
  type        = list(string)
}
variable "user_s3_access_key_secret" {
  description = "secret key for user access s3"
  type        = string
}

variable "user_s3_access_key_id" {
  description = "secret id for user access s3"
  type        = string
}

