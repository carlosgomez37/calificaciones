resource "aws_wafv2_web_acl" "my_web_acl" {
  name  = "${var.waf_name}_${var.layer}_${var.stack_id}"
  scope = "CLOUDFRONT"

  default_action {
    allow {}
  }

  rule {
    name     = "RateLimit"
    priority = 1

    action {
      block {}
    }

    statement {

      rate_based_statement {
        aggregate_key_type = "IP"
        limit              = 500
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "RateLimit"
      sampled_requests_enabled   = true
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = false
    metric_name                = "web_acl_${var.layer}_${var.stack_id}"
    sampled_requests_enabled   = false
  }

  tags = {
    Name   = "${var.waf_name}_${var.layer}_${var.stack_id}"
    source = "Terraform"
  }
}
