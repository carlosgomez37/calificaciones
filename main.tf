#Seccion VPC
module "calificaciones_network" {
  source = "./modules/vpc-global"
  layer  = var.layer
  stack_id = var.stack_id
  vpc_cidr = var.vpc_cidr
  subnets_private = [var.subnet1_cidr_private, var.subnet2_cidr_private, var.subnet3_cidr_private]
  subnets_public  = [var.subnet1_cidr_public, var.subnet2_cidr_public, var.subnet3_cidr_public]
}
output "vpc" {
  value = module.calificaciones_network
}
# Seccion s3 - cloudfront
module "calificaciones_s3_cloudfront" {
  source = "./modules/cloudfrontS3"
  bucket_name = "calificaciones-multimedia"
  layer       = var.layer
  stack_id    = var.stack_id
}
module "calificaciones_back_fargate_ecs" {
  source = "./modules/fargateBackECSALB"

  layer          = var.layer
  stack_id       = var.stack_id
  app_image      = var.app_image
  app_port       = var.app_port
  fargate_cpu    = var.fargate_cpu
  fargate_memory = var.fargate_memory
  region         = var.region
  app_count      = var.app_count
  ecr_repository = var.ecr_repository

  user_s3_access_key_secret = aws_iam_access_key.user_s3_access_key.secret
  user_s3_access_key_id     = aws_iam_access_key.user_s3_access_key.id

  db_subnets_public  = module.calificaciones_network.subnets_public
  db_subnets_private = module.calificaciones_network.subnets_private
  vpc                = module.calificaciones_network.vpc.id

}
# Cloudfront para ELB
resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  origin {
    domain_name = module.calificaciones_back_fargate_ecs.alb.dns_name
    origin_id   = "ALB-${module.calificaciones_back_fargate_ecs.alb.name}"

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  enabled         = true
  is_ipv6_enabled = false
  comment         = "Cloudfront_${var.layer}_${var.stack_id}"

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "ALB-${module.calificaciones_back_fargate_ecs.alb.name}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }

      headers = ["Host"]
    }

    compress    = true
    min_ttl     = 0
    default_ttl = 3600
    max_ttl     = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  tags = {
    Name   = "Cloudfront_microsites_${var.layer}_${var.stack_id}"
    source = "Terraform"
  }
  web_acl_id = module.calificaciones_blog_waf_web_acl_cloudfront.waf_reference.arn
}
# Seccion WAF v2 cloudfront
module "calificaciones_blog_waf_web_acl_cloudfront" {
  source = "./modules/wafV2Cloudfront"

  waf_name = "waf_main_blog_cloudfront"
  layer    = var.layer
  stack_id = var.stack_id
}

# IAM s3
resource "aws_iam_user" "user_s3_access" {
  name = "users3Access_${var.layer}_${var.stack_id}"

  tags = {
    environment = var.stack_id
    source      = "Terraform"
  }
}

resource "aws_iam_access_key" "user_s3_access_key" {
  user = aws_iam_user.user_s3_access.name
}

resource "aws_iam_user_policy_attachment" "user_s3_access_attach" {
  user       = aws_iam_user.user_s3_access.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

# RDS Aurora mysql serverless
module "calificaciones_rds_serverless_mysql" {
  source = "./modules/rdsServerlessAuroraMysql"

  cluster_identifier = var.cluster_identifier
  keypempub          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC4GTmzCv0Q7uRVrWa8BfKy0TV/LkOZay48PNJNvH9iEtx9OnBTla3Mua98QqVWBKaZkSzJhqnjQQD3kS7sKoWrFUoSXprXKerSweAJ7nTA7G+bf6gFApydHG1fSbUsTIH+Yvs6lzh5qBp2QwLzMmJqhhgpYCd6kS8uLCOzcZ3CJasvv2jus6aokkayeuZzUzET+ZAMgLsiHBLAMsMtrSKbNVZ6cdOAVRg72q7/iDQRbfBduMu3sqk1UcjTjatEgP5zvv97gcCi8/QEqaqBgER4sbvxXZzH6AGluVb8kUiaRwJnKoReZMX+s0OMpaRi0MfsV+sWkupXj99h97KZ9iNbDy7OS4ZMc/TdD+4zMfSli5awgG4Je9vUl+aQ3CKNb6qoU6mIknNUwEXfHwC6woYqHQjcTIMaow9xYpBulPuVFhm044S+UIVXeum81Z64+a6yb4BK2f9joTxQVvTYf+PA0qLfsMwaye+dDVjLymCgzIAFIXqiJtNlHKuZTA7wEwh0mvojypOJyBSO0AumiRTigsBzkuInRcN2pFf7PmqD2gPmzE8a4gvzwjrOgpE+5os7/2xydsFD2GyyxhgEhRCfhqO0cK0+3cVfITOQnpcUmNs4d8kFcgV5TgXWEuCBg/9G49NPSxp9GYkKoL2ZScibD4eQQ9br2Drnjx6jERtuqQ== joseluismontenegroromero@MacBook-Pro.local"
  layer              = var.layer
  stack_id           = var.stack_id
  db_name            = var.db_name
  db_user            = var.db_user
  db_password        = var.db_password
  db_subnets         = module.calificaciones_network.subnets_public
  vpc                = module.calificaciones_network.vpc.id
}


