# provider configiration

variable "provider_master" {
  type = map(any)
  default = {
    region  = "us-east-1"
    profile = "default"
  }
}

#project envs

variable "project" {
  type = map(any)
  default = {
    name        = "calificaciones"
    environment = "master"
  }
}
variable "stack_id" {
  description = "Nombre del ambiente"
  type        = string
  default     = "dev" 
}

variable "layer" {
  description = "Nombre del proyecto"
  type        = string
  default     = "calificaciones"
}

variable "type" {
  description = "Tipo del recurso, infra, frontend, movil, backend"
  type        = string
  default     = "infra" 
}

variable "component_name" {
  description = "Nombre del componente"
  type        = string
  default     = "core" 
}

variable "region" {
  default = "us-east-1"
}

variable "vpc_cidr" {
  type = string
  default = "10.66.88.0/24" 
}

variable "subnet1_cidr_public" {
  description = "CIDR subnet public connection red"
  type        = string
  default     = "10.66.88.192/28" 
}

variable "subnet2_cidr_public" {
  description = "CIDR subnet public connection red"
  type        = string
  default     = "10.66.88.208/28" 
}

variable "subnet3_cidr_public" {
  description = "CIDR subnet public connection red"
  type        = string
  default     = "10.66.88.224/28" 

}

variable "subnet1_cidr_private" {
  description = "CIDR subnet private connection red"
  type        = string
  default     = "10.66.88.0/26"
}

variable "subnet2_cidr_private" {
  description = "CIDR subnet private connection red"
  type        = string
  default     = "10.66.88.64/26" 
}

variable "subnet3_cidr_private" {
  description = "CIDR subnet private connection red"
  type        = string
  default     = "10.66.88.128/26"
}

variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  type        = string
  default     = "nginx" 
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  type        = number
  default     = 80 
}

variable "app_count" {
  description = "Number of docker containers to run"
  type        = number
  default     = 1  
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  type        = string
  default     = "2048" 
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  type        = string
  default     = "4096" 
}

variable "ecr_repository" {
  description = "ECR repositories for ECS"
  type        = list(string)
  default     = ["front_end"] 
}


variable "db_name" {
  description = "Name database"
  type        = string
  default     = "calificaciones_db" 
}

variable "db_user" {
  description = "Name user database"
  type        = string
  default     = "admin" 
}

variable "db_password" {
  description = "password database"
  type        = string
  default     = "12345678c" 
}

variable "cluster_identifier" {
  description = "Name cluster"
  type        = string
  default     = "calificaciones-dev" 
}